from django.forms import ModelForm
from .models import product_models
   
class Product_modelsForm(ModelForm):
    class Meta:
        model = product_models
        fields = ['product', 'description', 'amount','price', 'photo']
