# Storage system

# Instalações:
 - ## Python
   - Passo 1: Instale o python 3:
     -  sudo apt-get install python3.5

   - Passo 2: Instale o pip ( certifique-se que o python padrão é o 3, digite 'python' no terminal e veja a versão!):
     - sudo apt-get install python-pip

   - Passo 3: Caso o python padrão seja o 2 e você não queira mudar, utilize:
     - sudo apt-get install python3-pip
  
 - ## PyCharm
   - Passo 1: Abra o terminal (no Unity use as teclas CTRL + ALT + T):

   - Passo 2: Adicione o repositório do programa:
     - sudo add-apt-repository ppa:viktor-krivak/pycharm

   - Passo 3: Atualize o gerenciador de pacotes com o comando:
     - sudo apt-get update

   - Passo 4: instalar a IDE PyCharm básica:
     - sudo apt-get install pycharm
   
 - ## MySql
   - Passo 1: Atualize seu ambiente:
     - sudo apt update

   - Passo 2: Instale o servidor:
     - sudo apt install mysql-server

   - Passo 3: Realize a instalação segura de acordo com suas preferencias ( recomendado que saiba inglês básico ):
     - mysql_secure_installation
 
 - ## Mongo
   - Passo 1: Adicione o repositório:
     - sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

   - Passo 2: Execute o seguinte comando para criar um arquivo de lista para o MongoDB:
     - echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

   - Passo 3: Depois de adicionar os detalhes do repositório, precisamos atualizar a lista de pacotes:
     - sudo apt-get update

   - Passo 4: Instalar o pacote do MongoDB propriamente dito:
     - sudo apt-get install -y mongodb-org
 
   - Passo 5: Criar um arquivo de unidade para gerenciar o serviço MongoDB:
     - sudo nano /etc/systemd/system/mongodb.service
 
 - ## Mysql Workbench
   - Passo 1: Execute o comando:
     - sudo apt install mysql-workbench

 - ## Django versão 2.1.2
   - Documentação Django
     - https://docs.djangoproject.com/en/2.1/
   - Instalação
     - pip install Django==2.1.2