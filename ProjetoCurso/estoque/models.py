from django.db import models

class product_models(models.Model):
    product = models.CharField(max_length=50)
    description = models.TextField(max_length=300)
    amount = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    photo = models.ImageField(upload_to='product_photos', null=True, blank=True)

    def __str__(self):
        return self.product