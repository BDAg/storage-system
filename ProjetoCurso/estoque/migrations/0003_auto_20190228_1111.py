# Generated by Django 2.1.7 on 2019-02-28 11:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('estoque', '0002_product_models_photo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product_models',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
