from django.http import HttpResponse
from django.shortcuts import render

# =-=-=-=-=-=-= Exemplos =-=-=-=-=-=-=-=

def hello(request):
    return HttpResponse("Ola mundo")

def articles(request, year):
    return HttpResponse("Numero do ano: "+ str(year))


def home(request):
    return render(request, 'index.html')