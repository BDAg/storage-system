from django.urls import path
from .views import product_list
from .views import product_new
from .views import product_delete
from .views import product_update

urlpatterns = [
    path('list/', product_list, name='product_list'),
    path('new/', product_new, name='product_new'),
    path('delete/<int:id>/', product_delete, name='product_delete'),
    path('update/<int:id>/', product_update, name='product_update')
]
