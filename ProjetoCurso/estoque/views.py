from django.shortcuts import render ,redirect, get_object_or_404
from .models import product_models
from .forms import Product_modelsForm

def product_list(request):
    product = product_models.objects.all()
    return render(request,'product_list.html', {'Products' : product} ) 

def product_new(request):
    form = Product_modelsForm(request.POST or None, request.FILES or None)
    
    if form.is_valid():
        form.save()
        return redirect('product_list')
    return render(request, 'product_form.html', {'form': form})

def product_delete(request, id):
    product = get_object_or_404(product_models, pk=id)
    products = product_models.objects.all()
    
    if request.method == 'POST':
        product.delete()
        return redirect('product_list')

    
    return render(request, 'product_delete.html', {'product':product})

def product_update(request, id):
    product = get_object_or_404(product_models, pk=id)
    form = Product_modelsForm(request.POST or None, request.FILES or None, instance=product)

    if form.is_valid():
        form.save()
        return redirect('product_list')
    return render(request,'product_form.html', {'form': form})